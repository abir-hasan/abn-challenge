package com.abir.abn.challenge.ui.list.viewHolder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.abir.abn.challenge.R
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.databinding.AdapterRepoListBinding

class RepoViewHolder(
    private val binding: AdapterRepoListBinding,
    private val onRepoClick: ((Repo) -> Unit)
) :
    RecyclerView.ViewHolder(binding.root) {

    private var mRepo: Repo? = null

    companion object {
        fun create(parent: ViewGroup): AdapterRepoListBinding {
            return AdapterRepoListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        }
    }

    init {
        binding.root.setOnClickListener {
            mRepo?.let { onRepoClick.invoke(it) }
        }
    }

    fun bind(repo: Repo) {
        mRepo = repo
        binding.tvRepoName.text = repo.name
        binding.tvRepoVisibility.text = repo.visibility
        binding.ivAvatar.load(repo.ownerAvatarUrl) {
            transformations(CircleCropTransformation())
            crossfade(true)
            placeholder(R.drawable.ic_account_circle)
            error(R.drawable.ic_account_circle)
        }
        if (repo.isPrivate) {
            binding.ivPrivate.load(R.drawable.ic_repo_private)
        } else {
            binding.ivPrivate.load(R.drawable.ic_repo_public)
        }
    }

}