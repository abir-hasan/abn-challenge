package com.abir.abn.challenge.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.data.repository.AbnRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class RepoListViewModel @Inject constructor(abnRepository: AbnRepository) : ViewModel() {

    val reposFlow: StateFlow<PagingData<Repo>>

    init {
        reposFlow = abnRepository
            .getAbnReposStream()
            .cachedIn(viewModelScope)
            .stateIn(
                viewModelScope,
                SharingStarted.Lazily,
                PagingData.empty()
            )
    }

}