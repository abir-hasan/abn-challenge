package com.abir.abn.challenge.ui.list.viewHolder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.abir.abn.challenge.R
import com.abir.abn.challenge.databinding.AdapterReposLoadStateBinding
import java.net.UnknownHostException

class ReposLoadStateViewHolder(
    private val binding: AdapterReposLoadStateBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.btnRetry.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            if (loadState.error is UnknownHostException) {
                binding.tvErrorMessage.text =
                    binding.root.context.getString(R.string.no_internet_message)
            } else {
                binding.tvErrorMessage.text = loadState.error.localizedMessage
            }
        }
        binding.progressbar.isVisible = loadState is LoadState.Loading
        binding.btnRetry.isVisible = loadState is LoadState.Error
        binding.tvErrorMessage.isVisible = loadState is LoadState.Error
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): ReposLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_repos_load_state, parent, false)
            val binding = AdapterReposLoadStateBinding.bind(view)
            return ReposLoadStateViewHolder(binding, retry)
        }
    }
}