package com.abir.abn.challenge.data.pager

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.abir.abn.challenge.data.entities.RemoteKeys
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.data.local.AppDatabase
import com.abir.abn.challenge.data.model.RepositoriesModelItem
import com.abir.abn.challenge.data.remote.AbnReposService
import com.abir.abn.challenge.utils.logDebug
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class AbnReposRemoteMediator(
    private val service: AbnReposService,
    private val database: AppDatabase
) : RemoteMediator<Int, Repo>() {

    companion object {
        private const val TAG = "AbnReposPagingSource"
        private const val STARTING_PAGE_INDEX = 1
    }

    override suspend fun initialize(): InitializeAction {
        // Only refreshing data if there is no repo in database
        // Otherwise skipping the initial refresh load state
        return if (database.remoteKeysDao()
                .nextPage() != null
        ) InitializeAction.SKIP_INITIAL_REFRESH else
            InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, Repo>
    ): MediatorResult {
        // Determining in which state the pager is in
        val page: Int = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys: RemoteKeys? = getKeyClosestToCurrent(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                // Only getting forward data
                return MediatorResult.Success(endOfPaginationReached = true)
            }
            LoadType.APPEND -> {
                val nextKey = getNextPage()
                if (nextKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = true)
                }
                nextKey
            }
        }

        val pageSize = state.config.pageSize

        return try {
            val response: ArrayList<RepositoriesModelItem> =
                service.getRepos(page = page, perPage = pageSize)

            val repos: List<Repo> = parseRepoList(response)
            val endOfPaginationReached = repos.isEmpty()

            // DB Transaction
            database.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    // Clear all the tables in the database
                    database.remoteKeysDao().clearRemoteKeys()
                    database.reposDao().clearRepos()
                }

                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1

                val keys = repos.map {
                    RemoteKeys(repoId = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                database.remoteKeysDao().insertAll(keys)
                database.reposDao().insertAll(repos)
            }
            MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            MediatorResult.Error(exception)
        }
    }

    private fun parseRepoList(response: ArrayList<RepositoriesModelItem>) =
        response.map { item ->
            Repo(
                id = (item.id ?: 0).toLong(),
                name = item.name ?: "",
                fullName = item.fullName ?: "",
                description = item.description ?: "",
                ownerAvatarUrl = item.owner?.avatarUrl ?: "",
                visibility = item.visibility?.replaceFirstChar { it.uppercase() } ?: "",
                isPrivate = item.private ?: false,
                htmlUrl = item.htmlUrl ?: ""
            )
        }.toList()

    /**
     * Get nextKey of the page to APPEND
     */
    private suspend fun getNextPage(): Int? {
        return database.remoteKeysDao().nextPage()
    }

    private suspend fun getKeyClosestToCurrent(state: PagingState<Int, Repo>): RemoteKeys? {
        "getKeyClosestToCurrent()".logDebug(TAG)
        // Get item which is closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                database.remoteKeysDao().remoteKeysFromRepoId(repoId)
            }
        }
    }
}