package com.abir.abn.challenge.data.pager

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.data.model.RepositoriesModelItem
import com.abir.abn.challenge.data.remote.AbnReposService
import com.abir.abn.challenge.utils.logVerbose
import com.abir.abn.challenge.utils.logWarn
import retrofit2.HttpException
import java.io.IOException

class AbnReposPagingSource(private val service: AbnReposService) : PagingSource<Int, Repo>() {

    companion object {
        private const val TAG = "AbnReposPagingSource"
        const val STARTING_PAGE_INDEX = 1
    }

    override fun getRefreshKey(state: PagingState<Int, Repo>): Int {
        return STARTING_PAGE_INDEX
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Repo> {
        val page = params.key ?: STARTING_PAGE_INDEX
        val loadSize = params.loadSize
        "load() page: $page loadSize: $loadSize".logWarn(TAG)
        return try {
            val response: ArrayList<RepositoriesModelItem> =
                service.getRepos(page = page, perPage = loadSize)

            val repos: List<Repo> = response.map { item ->
                Repo(
                    id = (item.id ?: 0).toLong(),
                    name = item.name ?: "",
                    fullName = item.fullName ?: "",
                    description = item.description ?: "",
                    ownerAvatarUrl = item.owner?.avatarUrl ?: "",
                    visibility = item.visibility ?: "",
                    isPrivate = item.private ?: false,
                    htmlUrl = item.htmlUrl ?: ""
                )
            }.toList()

            "load() itemLoaded: ${repos.size}".logVerbose(TAG)
            val nextKey = if (repos.size < loadSize) null else page + 1

            LoadResult.Page(
                data = repos,
                nextKey = nextKey,
                prevKey = null // Only appending
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }


}