package com.abir.abn.challenge.ui.details

import androidx.lifecycle.ViewModel
import com.abir.abn.challenge.data.repository.AbnRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RepoDetailsViewModel @Inject constructor(repository: AbnRepository) : ViewModel() {
}