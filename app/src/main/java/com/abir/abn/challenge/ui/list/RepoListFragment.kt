package com.abir.abn.challenge.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.abir.abn.challenge.R
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.databinding.FragmentRepoListBinding
import com.abir.abn.challenge.ui.BaseFragment
import com.abir.abn.challenge.ui.details.RepoDetailsFragment
import com.abir.abn.challenge.ui.list.adapter.RepoListAdapter
import com.abir.abn.challenge.ui.list.adapter.ReposLoadStateAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RepoListFragment : BaseFragment() {

    private var _binding: FragmentRepoListBinding? = null

    private val binding: FragmentRepoListBinding
        get() = _binding!!

    private val viewModel: RepoListViewModel by viewModels()

    private lateinit var repoListAdapter: RepoListAdapter

    private var isOnline = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRepoListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUi()
        observePagerLoadStates()
        handleBackPress()
        setupObservers()
    }

    private fun setupObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.reposFlow.collectLatest { pagingData ->
                    repoListAdapter.submitData(pagingData)
                }
            }
        }
    }

    private fun setUpUi() {
        repoListAdapter = RepoListAdapter(::onRepoListItemClick)
        binding.rvRepos.layoutManager = LinearLayoutManager(requireContext())
        val footerAdapter = ReposLoadStateAdapter { repoListAdapter.retry() }
        binding.rvRepos.adapter = repoListAdapter.withLoadStateFooter(
            footer = footerAdapter
        )
    }

    private fun observePagerLoadStates() {
        lifecycleScope.launch {
            repoListAdapter.loadStateFlow.collect { loadState ->
                binding.rvRepos.isVisible =
                    loadState.source.refresh is LoadState.NotLoading ||
                            loadState.mediator?.refresh is LoadState.NotLoading

                // Spinner during initial load or refresh
                binding.progressbar.isVisible = loadState.mediator?.refresh is LoadState.Loading
                // Retry state if initial load or refresh fails
                val isListEmpty =
                    loadState.mediator?.refresh is LoadState.Error && repoListAdapter.itemCount == 0
                binding.tvEmptyList.isVisible = isListEmpty && !isOnline
                binding.tvEmptyList.text = getMessage(isListEmpty)
            }
        }
    }

    private fun getMessage(listEmpty: Boolean): String {
        val builder = StringBuilder()
        if (listEmpty)
            builder.append(getString(R.string.no_results))
        if (!isOnline) {
            builder.append("\n")
            builder.append(getString(R.string.no_internet_message))
        }
        return builder.toString()
    }

    private fun handleBackPress() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private fun onRepoListItemClick(repo: Repo) {
        val bundle = Bundle().apply {
            putParcelable(RepoDetailsFragment.KEY_REPO, repo)
        }
        findNavController().navigate(
            R.id.action_charactersFragment_to_characterDetailFragment,
            bundle
        )
    }

    override fun onNetworkStatusChange(isOnline: Boolean) {
        super.onNetworkStatusChange(isOnline)
        // Updating the cache, once the data is available
        this.isOnline = isOnline
        if (isOnline)
            repoListAdapter.retry()
    }
}