package com.abir.abn.challenge.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.abir.abn.challenge.data.entities.RemoteKeys

@Dao
interface RemoteKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKeys: List<RemoteKeys>)

    @Query("SELECT * FROM remote_keys WHERE repo_id = :repoId")
    suspend fun remoteKeysFromRepoId(repoId: Long): RemoteKeys?

    @Query("SELECT MAX(next_key) FROM remote_keys")
    suspend fun nextPage(): Int?

    @Query("DELETE FROM remote_keys")
    suspend fun clearRemoteKeys()
}