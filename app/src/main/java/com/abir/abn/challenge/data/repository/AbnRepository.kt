package com.abir.abn.challenge.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.data.local.AppDatabase
import com.abir.abn.challenge.data.pager.AbnReposRemoteMediator
import com.abir.abn.challenge.data.remote.AbnReposService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AbnRepository @Inject constructor(
    private val service: AbnReposService,
    private val database: AppDatabase
) {

    companion object {
        const val NETWORK_PAGE_SIZE = 10
    }

    /**
     * Setting up Repo stream through paging library
     * Through paging source we are establishing one point of truth with the database
     * And remote mediator handling the remote service connection and updating the database
     */
    fun getAbnReposStream(): Flow<PagingData<Repo>> {
        val pagingSourceFactory = { database.reposDao().getRepos() }
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                initialLoadSize = NETWORK_PAGE_SIZE,
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = AbnReposRemoteMediator(service, database),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

}