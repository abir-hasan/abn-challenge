package com.abir.abn.challenge.data.model


import com.google.gson.annotations.SerializedName

class RepositoriesResponse : ArrayList<RepositoriesModelItem>()