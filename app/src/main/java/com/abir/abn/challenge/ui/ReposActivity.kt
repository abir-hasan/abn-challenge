package com.abir.abn.challenge.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.abir.abn.challenge.R
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.databinding.ActivityReposBinding
import com.abir.abn.challenge.ui.details.RepoDetailsFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReposActivity : AppCompatActivity() {

    private lateinit var binding: ActivityReposBinding

    companion object {
        private const val TAG = "ReposActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReposBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        val navController: NavController = navHostFragment.navController

        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        setupDynamicTitle(navController)
    }

    private fun setupDynamicTitle(navController: NavController) {
        navController.addOnDestinationChangedListener { _, destination, argument ->
            when (destination.id) {
                R.id.repoDetailsFragment -> {
                    val repo = argument?.getParcelable<Repo>(RepoDetailsFragment.KEY_REPO)
                    binding.toolbar.title = repo?.name
                }
            }
        }
    }

}