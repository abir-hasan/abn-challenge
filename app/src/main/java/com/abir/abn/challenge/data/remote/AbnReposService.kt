package com.abir.abn.challenge.data.remote

import com.abir.abn.challenge.data.model.RepositoriesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface AbnReposService {

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }

    @GET("users/abnamrocoesd/repos")
    suspend fun getRepos(
        @Query("page") page: Int = 1,
        @Query("per_page") perPage: Int = 10
    ): RepositoriesResponse

}