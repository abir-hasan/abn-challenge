package com.abir.abn.challenge.data.local

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.abir.abn.challenge.data.entities.Repo

@Dao
interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(repos: List<Repo>)

    @Query("SELECT * FROM repos")
    fun getRepos(): PagingSource<Int, Repo>

    @Query("DELETE FROM repos")
    suspend fun clearRepos()

}