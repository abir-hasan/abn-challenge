package com.abir.abn.challenge.ui.details

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import coil.load
import coil.transform.CircleCropTransformation
import com.abir.abn.challenge.R
import com.abir.abn.challenge.data.entities.Repo
import com.abir.abn.challenge.databinding.FragmentRepoDetailsBinding
import com.abir.abn.challenge.utils.logError
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RepoDetailsFragment : Fragment() {

    companion object {
        private const val TAG = "RepoDetailsFragment"
        const val KEY_REPO = "KEY_REPO"
    }

    private var _binding: FragmentRepoDetailsBinding? = null

    private val binding: FragmentRepoDetailsBinding
        get() = _binding!!

    private var repo: Repo? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRepoDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        "onDestroyView() called".logError(TAG)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getRepoDetails()
        setUpUi()
    }

    private fun setUpUi() {
        repo?.let { model ->
            binding.tvFullName.text = model.fullName
            binding.tvDescription.text = model.description
            binding.ivAvatar.load(model.ownerAvatarUrl) {
                transformations(CircleCropTransformation())
                crossfade(true)
                placeholder(R.drawable.ic_account_circle)
            }
            binding.btnOpenRepo.setOnClickListener { openRepositoryInBrowser(model.htmlUrl) }
            binding.tvVisibility.text = getString(R.string.visibility, model.visibility)
            if (model.isPrivate) {
                binding.ivPrivate.load(R.drawable.ic_repo_private)
            } else {
                binding.ivPrivate.load(R.drawable.ic_repo_public)
            }
        }
    }

    @Throws(ActivityNotFoundException::class)
    private fun openRepositoryInBrowser(htmlUrl: String) {
        val repoUrl = Uri.parse(htmlUrl)
        val intent = Intent(Intent.ACTION_VIEW, repoUrl)
        startActivity(intent)
    }

    private fun getRepoDetails() {
        repo = arguments?.getParcelable(KEY_REPO)
    }
}